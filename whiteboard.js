var settings = require('./public/util/Settings.js'),
    tests = require('./public/util/tests.js'),
    draw = require('./public/util/draw.js'),
    projects = require('./public/util/projects.js'),
    db = require('./public/util/db.js'),
    express = require('express'),
    busboy = require('connect-busboy')
    paper = require('paper'),
    socket = require('socket.io'),
    async = require('async'),
    fs = require('fs'),
    http = require('http'),
    https = require('https'),
    redis = require('redis'),
    cookieParser = require('cookie-parser'),
    session = require('express-session'),
    errorHandler = require('errorhandler'),
    Canvas = require('canvas');


/** 
 * SSL Logic and Server bindings
 */ 
if(settings.ssl){
  console.log("SSL Enabled");
  console.log("SSL Key File" + settings.ssl.key);
  console.log("SSL Cert Auth File" + settings.ssl.cert);

  var options = {
    key: fs.readFileSync(settings.ssl.key),
    cert: fs.readFileSync(settings.ssl.cert)
  };
  var app = express(options);
  var server = https.createServer(options, app).listen(settings.port);
}else{
  var app = express();
  var server = app.listen(settings.port);
}

/** 
 * Build Client Settings that we will send to the client
 */
var clientSettings = {
  "tool": settings.tool
};

var env = process.env.NODE_ENV || 'development';

app.use(express.static(__dirname + '/'));

// Sessions
app.use(cookieParser());
app.use(session({
  secret: 'secret',
  key: 'express.sid',
  resave: true,
  saveUninitialized: true})
);

// Development mode setting
if ('development' == env) {
  app.use(errorHandler({ dumpExceptions: true, showStack: true }));
}

// Production mode setting
if ('production' == env) {
  app.use(errorHandler());
}

//setup view engine
app.set('view engine', 'pug');
app.set('views', __dirname + '/views');

// ROUTES
// Index page
app.get('/', function(req, res){
  res.render('index.pug');
});

// Drawings
app.get('/whiteboard/*', function(req, res){
  res.render('whiteboard.pug');
});

//404 ERROR PAGE
app.get('/error', function(req, res){
  res.render('error.pug');
});

app.use(busboy());

// Front-end tests
app.get('/tests/frontend/specs_list.js', function(req, res){
  tests.specsList(function(tests){
    res.send("var specs_list = " + JSON.stringify(tests) + ";\n");
  });
});

// Used for front-end tests
app.get('/tests/frontend', function (req, res) {
  res.redirect('/tests/frontend/');
});

// Testing PDF generation
app.get('/tests/pdf-generation', function (req, res) {
  var room = req.query.room;
  var Image = Canvas.Image;
  var canvas = new Canvas(1366, 612, 'pdf');
  var ctx = canvas.getContext('2d');

  var x, y;

  function reset () {
    x = 0;
    y = 0;
  }
  function img (src) {
    var img = new Image();
    img.src = src;
    ctx.drawImage(img, 0, 0, img.width, img.height);
  }

  db.getPDFPageCount(room,function(count){
    var receivedPageCount = 0;
    for(var x = 1; x <= count; x++){
      db.getPDFPage(room,x, function(page){
        receivedPageCount++;
        reset();
        img(page);
        ctx.addPage();
        if(receivedPageCount == count){
          fs.writeFile('out.pdf', canvas.toBuffer(), function (err) {
            if (err) throw err;
            console.log('created out.pdf')
          });
        }
      });
    }
  });
});

// Static files IE Javascript and CSS
//app.use("/static", express.static(__dirname + '/public/static'));
app.use("/wb_assets/static", express.static(__dirname + '/public/static'));
app.use("/build", express.static(__dirname + '/public/static/pdfjs/build'));
app.use("/web", express.static(__dirname + '/public/static/pdfjs/web'));
app.use("/files", express.static(__dirname + '/user_files/asanka'));


// LISTEN FOR REQUESTS
var io = socket.listen(server);
io.sockets.setMaxListeners(0);

console.log("Access whiteboard at http://127.0.0.1:"+settings.port);

// SOCKET IO
io.sockets.on('connection', function (socket) {
  console.log("Socket connected: "+ socket);
  socket.on('disconnect', function () {
    console.log("Socket disconnected: " + socket);
    // TODO: We should have logic here to remove a drawing from memory as we did previously
  });

  // EVENT: User stops drawing something
  // Having room as a parameter is not good for secure rooms
  socket.on('draw:progress', function (room, uid, co_ordinates, stroke_width, pageNum) {
    if (!projects.projects[room] || !projects.projects[room].project) {
      loadError(socket);
      return;
    }
    io.in(room).emit('draw:progress', uid, co_ordinates, stroke_width);
    draw.progressExternalPath(room, JSON.parse(co_ordinates), uid, stroke_width, pageNum);
  });

  // EVENT: User stops drawing something
  // Having room as a parameter is not good for secure rooms
  socket.on('draw:end', function (room, uid, co_ordinates, pageNum) {
    if (!projects.projects[room] || !projects.projects[room].project) {
      loadError(socket);
      return;
    }
    io.in(room).emit('draw:end', uid, co_ordinates);
    draw.endExternalPath(room, JSON.parse(co_ordinates), uid, pageNum);
  });

  // User joins a room
  socket.on('subscribe', function(data) {
    subscribe(socket, data);
  });

  // User loads new page
  socket.on('load:newPage', function(room, pageNum, canvasClearedCount) {
    if (!projects.projects[room] || !projects.projects[room].project) {
      loadError(socket);
      return;
    }
    draw.setupNewPage(room, pageNum);
    var state = {
      "type": "WHITEBOARD",
      "page": Number(pageNum)+1
    };
    db.updateLatestState(room, state);
    io.in(room).emit('load:newPage', pageNum, canvasClearedCount); // emit back the new-page event so both teacher and student will be in sync
  });

  // Load a previous page
  socket.on('load:previousPage', function(room, pageNum) {
    draw.cleanRedoStack(room);
    var state = {
      "type": "WHITEBOARD",
      "page": Number(pageNum)
    };
    db.updateLatestState(room, state);
    db.loadPreviousPage(room, pageNum, function(project){
        io.sockets.in(room).emit('load:previousPage', project, pageNum);
    });
  });

  // scroll page
  socket.on('scroll', function(room,uid, yPosition) {
      io.sockets.in(room).emit('scroll', uid,yPosition);
  });
  // User clear
  socket.on('clear', function(room, uid, pageNum) {
    if (!projects.projects[room] || !projects.projects[room].project) {
      loadError(socket);
      return;
    }
    draw.clear(room, pageNum);
    io.in(room).emit('clear', uid); // emit back the cleared count so both teacher and student will be in sync
  });

  // User removes an item
  socket.on('item:remove', function(room, uid, itemName, pageNum) {
    draw.removeItem(room, uid, itemName, pageNum);
    io.sockets.in(room).emit('item:remove', uid, itemName);
  });

  // User moves one or more items on their canvas - progress
  socket.on('item:move:progress', function(room, uid, itemNames, delta) {
    draw.moveItemsProgress(room, uid, itemNames, delta);
      if (itemNames) {
        io.sockets.in(room).emit('item:move', uid, itemNames, delta);
    }
  });

  // User moves one or more items on their canvas - end
  socket.on('item:move:end', function(room, uid, itemNames, delta, pageNum) {
    draw.moveItemsEnd(room, uid, itemNames, delta, pageNum);
    if (itemNames) {
      io.sockets.in(room).emit('item:move', uid, itemNames, delta);
    }
  });

  // User adds a raster image
  socket.on('image:add', function(room, uid, data, position, name, pageNum) {
    draw.addImage(room, uid, data, position, name, pageNum);
    io.sockets.in(room).emit('image:add', uid, data, position, name);
  });

  // User performs UNDO
  socket.on('undo', function(room, uid, pageNum) {
    draw.undoItem(room, pageNum);
    io.sockets.in(room).emit('undo', uid);
  });

  // User performs REDO
  socket.on('redo', function(room, uid, pageNum) {
    draw.redoItem(room, pageNum);
    io.sockets.in(room).emit('redo', uid);
  });

  // User performs Resizing image
  socket.on('image:resize', function(room, uid, image, scalingFactor, pageNum) {
    draw.resizeImage(room,image,scalingFactor, pageNum);
    io.sockets.in(room).emit('image:resize', uid, image, scalingFactor);
  });

  // Start using pointer tool. show writer's cursor to other party of the class
  socket.on('pointing:start', function(room, uid, position) {
    io.sockets.in(room).emit('pointing:start', uid, position);
  });

  // End using pointer tool. hide writer's cursor from other party of the class
  socket.on('pointing:end', function(room, uid) {
    io.sockets.in(room).emit('pointing:end', uid);
  });

  // add text 'add:textbox', room, uid, text, currentPageNumber
  socket.on('add:textbox', function(room, uid, text, fontColor, fontSize, position, name, pgNum) {
    draw.addText(room, uid, text, position, fontColor, fontSize, name, pgNum);
    io.sockets.in(room).emit('add:textbox', uid, text, fontColor, fontSize, position, name);
  });

  // Enable toolbox
  socket.on('enable:toolbox', function(room, uid) {
    io.sockets.in(room).emit('enable:toolbox', uid);
  });

  // Disable toolbox
  socket.on('disable:toolbox', function(room, uid) {
    io.sockets.in(room).emit('disable:toolbox', uid);
  });

});
var roomUsers;
// Subscribe a client to a room
subscribe = (socket, data) => {
  var room = data.room;
  var user = data.username;

  // Subscribe the client to the room
  socket.join(room);
  socket.join(user);
  
  // declare a redo stack for classroom of it is not declared
  if(!draw.hasDeclaredRedoStack(room))
      draw.initRedoStack(room);

  // Create Paperjs instance for this room if it doesn't exist
  var project = projects.projects[room];
 
  if (!project) {
    console.log("made room");
    
    projects.projects[room] = {};
    projects.projects[room].project = new paper.Project();
    projects.projects[room].external_paths = {};
    db.load(room, socket);
  } else { // Project exists in memory, no need to load from database
    db.loadFromMemoryOrDB(room, socket, clientSettings);
  }

  roomUsers = retrieveConnectedUser(socket, room);
  io.to(room).emit('user:connect', roomUsers);
}

retrieveConnectedUser = (socket, _room) => {
  // Put all connected users in a list
  return Object.entries(socket.adapter.sids).map(
    (element) => {
      var sid = Object.getOwnPropertyDescriptors(element[1]);
      return (Object.keys(sid).length == 3) ? { room: Object.keys(sid)[1], name: Object.keys(sid)[2] } : null;
    })
    .filter(
      (element) => {
        return element && element.room === _room;
      });
}

loadError = (socket) => {
  socket.emit('project:load:error');
}
