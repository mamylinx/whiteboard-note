tool.minDistance = 1;
tool.maxDistance = 45;
var scaleFactor = 1.5;
var scalePercent = 1;
var send_paths_timer;
var timer_is_active = false;
var paper_object_count = 0;
var activeTool = "pencil";
var mouseTimer = 0; // used for getting if the mouse is being held down but not dragged IE when bringin up color picker
var mouseHeld; // global timer for if mouse is held.
var shapeStartPoint;
var shapeEndPoint;
var currentRatio = 1;
var selectToolMode = "ITEM_DRAG";
var _strokeWidth;

room = window.location.pathname.split("/")[2];

username = getParameterByName('username');

var redoStack = new Array(); // stack to store undo items
var pageCount = 1; // keep track of number of times the canvas cleared, so we can override the correct previous page at db
var currentPageNumber = 1; // when a previous page is loaded, this value should be the previous-page number.
/*
 * 0 - latest page
 * 1,2,3,4,5 - previous page
 */
var textBoxCoordinate; // coordinate of the text box. once user complete the required content in text box, draw a pointText on this coordinate

function buildRGBAColor(r, g, b, a) {
    r = Math.round(r * 255).toString(16);
    g = Math.round(g * 255).toString(16);
    b = Math.round(b * 255).toString(16);
    a = Math.round(a * 255).toString(16);

    if (r.length == 1)
        r = "0" + r;
    if (g.length == 1)
        g = "0" + g;
    if (b.length == 1)
        b = "0" + b;
    if (a.length == 1)
        a = "0" + a;

    return "#" + r + g + b + a;
}
/*http://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb*/
function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

// Get the username from the provided url

uid = username;

//emit page scrolling event
$('#subContainer').scroll(emitScrollEvent);
var previousYPosition = 0;

function emitScrollEvent() {
    var yPosition = $('#subContainer').scrollTop();
    if ((yPosition - previousYPosition > 50) || (yPosition - previousYPosition < -50)) {
        socket.emit('scroll', room, uid, yPosition);
        previousYPosition = yPosition;
    }
}

function updateStylingFromTools(_elt, _newBg) {
    $(_elt).css('background-image', 'url(/wb_assets/static/img/' + _newBg + '.png)');
}

function pickColor(color) {
    $('#color').val(color);
    var rgb = hexToRgb(color);
    $('#activeColorSwatch').css('background-color', 'rgb(' + rgb.r + ',' + rgb.g + ',' + rgb.b + ')');
    update_active_color();
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.search);
    if (results == null) {
        return "";
    } else {
        return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}

function deleteSelectedItem() {
    // Delete selected items
    var items = paper.project.selectedItems;
    if (items) {
        for (x in items) {
            var itemToRemove = items[x];
            socket.emit('item:remove', room, uid, itemToRemove.name, currentPageNumber);
            itemToRemove.remove();
            view.draw();
        }
    }
}

$(document).ready(function() {
    // initialize toolbox
    $('.tool-box').css({
        "display": "block"
    });
    $('.tool-box').addClass('animated zoomIn');

    var btns = document.getElementsByClassName("dropdown-item");

    // Loop through the buttons and add the active class to the current/clicked button
    for (var i = 0; i < btns.length; i++) {
        btns[i].addEventListener("click", function() {
            var current = document.getElementsByClassName("active");
            current[0].className = current[0].className.replace(" active", "");
            this.className += " active";
        });
    }

    if (paper.project.activeLayer.hasChildren()) // notifying user that he can undo now
        $('.buttonicon-undo').addClass('disabled');

    $('#tip').on('input', function() {
        if ($('#tip').val() > 100) {
            $(this).after("<span class='text-danger small'>Tip size must be less than 100</span>");
            $('#tip').val(5);
        } else {
            $('span').remove('.text-danger');
        }
    });
});

// Join the room
socket.emit('subscribe', {
    room: room,
    username: username
});

// JSON data of the users current drawing
// Is sent to the user
var path_to_send = {};

// Calculates colors
var active_color_rgb;
var active_color_json = {};
var opacity = 1;
$(document).on('input change', '#opacityRangeVal', function() {
    opacity = $(this).val() / 255;
})


var update_active_color = function() {
    var rgb_array = $('#activeColorSwatch').css('background-color');
    $('#editbar').css("border-bottom", "solid 2px " + rgb_array);
    var a,
        rgb = rgb_array.replace(/\s/g, '').match(/^rgba?\((\d+),(\d+),(\d+),?([^,\s)]+)?/i),
        hex = rgb ?
        (rgb[1] | 1 << 8).toString(16).slice(1) +
        (rgb[2] | 1 << 8).toString(16).slice(1) +
        (rgb[3] | 1 << 8).toString(16).slice(1) : rgb_array;

    a = (opacity !== "") ? opacity : 1;

    _a = a;
    // multiply before convert to HEX
    a = ((a * 255) | 1 << 8).toString(16).slice(1)

    var red = rgb[1];
    var green = rgb[2];
    var blue = rgb[3];

    active_color_rgb = '#' + hex + a;

    active_color_json = {
        "red": red / 255 || 0,
        "green": green / 255,
        "blue": blue / 255,
        "opacity": _a
    };
};

update_active_color();

// ---------------------------------
// DRAWING EVENTS
var resizeVector, moving, item, newVector, defaultCenter = paper.view.center,
    offset = 0;

function onMouseDown(event) {
    $('#tipInput').hide(); //Hide tip size input when click on canvas
    if (activeTool === "pencil") {
        _strokeWidth = $('#tip').val() ? $('#tip').val() : 5;
    }
    if (activeTool === "eraser") {
        _strokeWidth = $('#tip').val() ? $('#tip').val() : 50;
    }

    if (event.which === 2) return; // If it's middle mouse button do nothing -- This will be reserved for panning in the future.
    $('.popup').fadeOut();

    // Ignore middle or right mouse button clicks for now
    if (event.event.button == 1 || event.event.button == 2) {
        return;
    }

    if (activeTool == "draw" || activeTool == "pencil" || activeTool == "eraser" || activeTool == "line" || activeTool == "rectangle" || activeTool == "triangle" || activeTool == "circle") {
        // The data we will send every 100ms on mouse drag

        var point = event.point;
        path = new Path();
        path.add(event.point);
        path.name = uid + ":" + (++paper_object_count);
        path_to_send = {
            name: path.name,
            rgba: active_color_json,
            start: event.point,
            path: [],
            tool: activeTool
        };
        if (activeTool == "draw") {
            path.fillColor = active_color_rgb;
        } else if (activeTool == "pencil") {
            path.strokeColor = active_color_rgb;
            path.strokeWidth = _strokeWidth;
        } else if (activeTool == "eraser") {
            path.strokeColor = buildRGBAColor(1, 1, 1, 1);
            path.strokeWidth = _strokeWidth; // increase this size for a larger eraser
            // The data we will send every 100ms on mouse drag
            path.blendMode = 'destination-out';
            path_to_send = {
                name: path.name,
                rgba: {
                    "red": 1,
                    "green": 1,
                    "blue": 1,
                    "opacity": 1
                },
                start: event.point,
                path: [],
                tool: activeTool
            };
        } else if (activeTool == "line" || activeTool == "rectangle" || activeTool == "triangle" || activeTool == "circle") {
            shapeStartPoint = point;
        }
        view.draw();
    } else if (activeTool == "select") {
        // Select item
        if (event.item) {
            item = event.item;
            // If holding shift key down, don't clear selection - allows multiple selections
            if (!event.event.shiftKey) {
                paper.project.activeLayer.selected = false;
            }

            item.selected = true;
            var cornerHit = item.hitTest(event.point, {
                bounds: true,
                tolerance: 10
            });
            // if a hit is detected on one of the corners...
            if (cornerHit && ['top-left', 'top-right', 'bottom-left', 'bottom-right'].indexOf(cornerHit.name) >= 0) {
                selectToolMode = 'ITEM_RESIZE'
                // ...store current vector from item center to point
                resizeVector = event.point - item.bounds.center;
                // ...else if hit is detected inside item...
            } else if (item.hitTest(event.point, {
                    fill: true
                })) {
                selectToolMode = 'ITEM_DRAG'
                // ...store moving state
                moving = true;
            }
            view.draw();
        } else {
            paper.project.activeLayer.selected = false;
        }
    }

    // send the position of cursor to other party of the class
    else if (activeTool == "point") {
        socket.emit('pointing:start', room, uid, event.point);
    }

    //zoom
    else if (activeTool == "zoom-in") {
        if (scalePercent < 15) {
            view.scale(scaleFactor, event.point);
            scalePercent *= scaleFactor;
            document.getElementById('scale-percent').textContent = scalePercent * 100 + "%";
        }
    } else if (activeTool == "zoom-out") {
        if (scalePercent > 0.5) {
            view.scale(1 / scaleFactor, event.point);
            scalePercent /= scaleFactor;
            document.getElementById('scale-percent').textContent = scalePercent * 100 + "%";
        }
    } else if (activeTool == "textBox") {
        textBoxCoordinate = event.point;
        if (textBoxCoordinate.x > window.innerWidth - 300) {
            $('#whiteboard-text-box-container').css({
                "top": Math.floor(textBoxCoordinate.y * 0.3) + 'px',
                "left": textBoxCoordinate.x - 380 + 'px',
                "display": "block"
            });
        } else {
            $('#whiteboard-text-box-container').css({
                "top": Math.floor(textBoxCoordinate.y * 0.3) + 'px',
                "left": textBoxCoordinate.x + 'px',
                "display": "block"
            });
        }
    }
}

var item_move_delta;
var send_item_move_timer;
var item_move_timer_is_active = false;

function onMouseDrag(event) {
    if (activeTool == "pencil") {
        _strokeWidth = $('#tip').val() ? $('#tip').val() : 5;
    }
    if (activeTool == "eraser") {
        _strokeWidth = $('#tip').val() ? $('#tip').val() : 50;
    }

    mouseTimer = 0;
    clearInterval(mouseHeld);

    // Ignore middle or right mouse button clicks for now
    if (event.event.button == 1 || event.event.button == 2) {
        return;
    }

    if (activeTool == "draw" || activeTool == "pencil" || activeTool == "eraser" || activeTool == "line" || activeTool == "rectangle" || activeTool == "triangle" || activeTool == "circle") {
        var step = event.delta / 2;
        step.angle += 90;
        if (activeTool == "draw") {
            var top = event.middlePoint + step;
            var bottom = event.middlePoint - step;
        } else if (activeTool == "pencil") {
            var top = event.middlePoint;
            bottom = event.middlePoint;
        } else if (activeTool == "eraser") {
            var top = event.middlePoint + 10; // 10 is added since clicking point is taken as the top left corner of cursor_eraser
            var bottom = event.middlePoint + 10; // increase this size appropriately for a larger eraser
        }

        //path.smooth();
        if (activeTool == "line") {
            paper.project.activeLayer.lastChild.remove();
            shapeEndPoint = event.point;
            path = new Path.Line(shapeStartPoint, shapeEndPoint);
            path.name = uid + ":" + (paper_object_count);
            path.strokeColor = active_color_rgb;
            path.strokeWidth = 2;
            path_to_send.path = {
                start: shapeStartPoint,
                end: shapeEndPoint
            };
        } else if (activeTool == "rectangle") {
            paper.project.activeLayer.lastChild.remove();
            shapeEndPoint = event.point;
            path = new Path.Rectangle(shapeStartPoint, shapeEndPoint);
            path.name = uid + ":" + (paper_object_count);
            path.strokeColor = active_color_rgb;
            path.strokeWidth = 2;
            path_to_send.path = {
                start: shapeStartPoint,
                end: shapeEndPoint
            };
        } else if (activeTool == "triangle") {
            paper.project.activeLayer.lastChild.remove();
            shapeEndPoint = event.point;
            path = new Path();
            path.add(shapeStartPoint);
            path.add(shapeEndPoint);
            path.add(new Point(2 * shapeStartPoint.x - shapeEndPoint.x, shapeEndPoint.y));
            path.closed = true;
            path.name = uid + ":" + (paper_object_count);
            path.strokeColor = active_color_rgb;
            path.strokeWidth = 2;
            path_to_send.path = {
                start: shapeStartPoint,
                end: shapeEndPoint
            };
        } else if (activeTool == "circle") {
            paper.project.activeLayer.lastChild.remove();
            shapeEndPoint = event.point;
            path = new Path.Circle(new Point((shapeStartPoint.x + shapeEndPoint.x) / 2, (shapeStartPoint.y + shapeEndPoint.y) / 2), (shapeStartPoint - shapeEndPoint).length / 2);
            path.name = uid + ":" + (paper_object_count);
            path.strokeColor = active_color_rgb;
            path.strokeWidth = 2;
            path_to_send.path = {
                start: shapeStartPoint,
                end: shapeEndPoint
            };
        } else {
            path.add(top);
            path.insert(0, bottom);
            // Add data to path
            path_to_send.path.push({
                top: top,
                bottom: bottom
            });
        }
        view.draw();


        // Send paths every 100ms
        if (!timer_is_active) {
            send_paths_timer = setInterval(function() {
                if ((activeTool != "line" && activeTool != "rectangle" && activeTool != "triangle" && activeTool != "circle") || path_to_send.path.start) {
                    socket.emit('draw:progress', room, uid, JSON.stringify(path_to_send), _strokeWidth, currentPageNumber);
                } else {
                    console.log("not send");
                }
                path_to_send.path = new Array();
            }, 100);

        }

        timer_is_active = true;
    } else if (activeTool == "select") {
        if (selectToolMode === 'ITEM_RESIZE' && resizeVector) {
            // ...calculate new vector from item center to point
            newVector = event.point - item.bounds.center;
            // scale item so current mouse position is corner position
            currentRatio = (newVector / resizeVector).length;

            if (newVector.length < resizeVector.length) {
                item.scale(1 - currentRatio * 0.03);
                socket.emit('image:resize', room, uid, item.name, 1 - currentRatio * 0.08, currentPageNumber);
            } else {
                item.scale(1 + currentRatio * 0.03);
                socket.emit('image:resize', room, uid, item.name, 1 + currentRatio * 0.08, currentPageNumber);
            }

            // store vector for next event
            resizeVector = newVector;
            // ...if item fill was previously hit...
        } else {
            // ...move item
            item.position += event.delta;
        }

        // Store delta
        if (paper.project.selectedItems) {
            if (!item_move_delta) {
                item_move_delta = event.delta;
            } else {
                item_move_delta += event.delta;
            }
        }

        // Send move updates every 50 ms
        if (!item_move_timer_is_active) {
            send_item_move_timer = setInterval(function() {
                if (item_move_delta && selectToolMode === 'ITEM_DRAG') {
                    var itemNames = new Array();
                    for (x in paper.project.selectedItems) {
                        itemNames.push(paper.project.selectedItems[x].name);
                    }
                    socket.emit('item:move:progress', room, uid, itemNames, item_move_delta);
                    item_move_delta = null;
                }
            }, 50);
        }
        item_move_timer_is_active = true;
    }
    // send the position of cursor to other party of the class
    if (activeTool == "point") {
        socket.emit('pointing:start', room, uid, event.point);
    }
}


function onMouseUp(event) {
    if (activeTool == "pencil") {
        _strokeWidth = $('#tip').val() ? $('#tip').val() : 5;
    }
    if (activeTool == "eraser") {
        _strokeWidth = $('#tip').val() ? $('#tip').val() : 50;
    }
    if (paper.project.activeLayer.hasChildren()) // notifying user that he can undo now
        $('.buttonicon-undo').removeClass('disabled');

    if (activeTool != 'undo' && activeTool != 'redo' && redoStack.length > 0) { // clearing redo stack after user restarts drawing
        $('.buttonicon-redo').addClass('disabled'); // notifying user that he can't redo now
        redoStack.length = 0;
    }

    // Ignore middle or right mouse button clicks for now
    if (event.event.button == 1 || event.event.button == 2) {
        return;
    }

    clearInterval(mouseHeld);

    if (activeTool == "line" || activeTool == "rectangle" || activeTool == "triangle" || activeTool == "circle") {
        shapeEndPoint = event.point;
        path_to_send.path = {
            start: shapeStartPoint,
            end: shapeEndPoint
        };
        path.closed = true;
        view.draw();

        socket.emit('draw:progress', room, uid, JSON.stringify(path_to_send), 2, currentPageNumber);
        socket.emit('draw:end', room, uid, JSON.stringify(path_to_send), currentPageNumber);

        // Stop new path data being added & sent
        clearInterval(send_paths_timer);
        path_to_send.path = new Array();
        timer_is_active = false;
    }
    if (activeTool == "draw" || activeTool == "pencil" || activeTool == "eraser") {
        // Close the users path
        path.add(event.point);
        path.closed = true;
        //path.smooth();
        view.draw();

        // Send the path to other users
        path_to_send.end = event.point;
        // This covers the case where paths are created in less than 100 seconds
        // it does add a duplicate segment, but that is okay for now.        
        socket.emit('draw:progress', room, uid, JSON.stringify(path_to_send), _strokeWidth, currentPageNumber);
        socket.emit('draw:end', room, uid, JSON.stringify(path_to_send), currentPageNumber);

        // Stop new path data being added & sent
        clearInterval(send_paths_timer);
        path_to_send.path = new Array();
        timer_is_active = false;
    }
    if (activeTool === "select") {
        if (paper.project.selectedItems.length > 0) {
            $('#deleteTool').show();
        }
        // End movement timer
        clearInterval(send_item_move_timer);
        if (selectToolMode === 'ITEM_DRAG') {
            // Send any remaining movement info
            var itemNames = new Array();
            for (x in paper.project.selectedItems) {
                itemNames.push(paper.project.selectedItems[x].name);
            }
            (item_move_delta) ? socket.emit('item:move:end', room, uid, itemNames, item_move_delta, currentPageNumber): socket.emit('item:move:end', room, uid, itemNames, new Point(0, 0), currentPageNumber);

        } else if (selectToolMode === 'ITEM_RESIZE' && resizeVector) {
            newVector = event.point - item.bounds.center;
            if (newVector.length < resizeVector.length) {
                item.scale(1 - currentRatio * 0.03);
                socket.emit('image:resize', room, uid, item.name, 1 - currentRatio * 0.08, currentPageNumber);
            } else {
                item.scale(1 + currentRatio * 0.03);
                socket.emit('image:resize', room, uid, item.name, 1 + currentRatio * 0.08, currentPageNumber);
            }
        }

        item_move_delta = null;
        item_move_timer_is_active = false;

        // ... reset state
        resizeVector = null;
        moving = null;
    }

    if (activeTool == "point") {
        socket.emit('pointing:end', room, uid);
    }

}

var key_move_delta;
var send_key_move_timer;
var key_move_timer_is_active = false;

function onKeyDown(event) {
    if (activeTool == "select") {
        var point = null;

        if (event.key == "up") {
            point = new paper.Point(0, -1);
        } else if (event.key == "down") {
            point = new paper.Point(0, 1);
        } else if (event.key == "left") {
            point = new paper.Point(-1, 0);
        } else if (event.key == "right") {
            point = new paper.Point(1, 0);
        }

        // Move objects 1 pixel with arrow keys
        if (point) {
            moveItemsBy1Pixel(point);
        }

        // Store delta
        if (paper.project.selectedItems && point) {
            if (!key_move_delta) {
                key_move_delta = point;
            } else {
                key_move_delta += point;
            }
        }

        // Send move updates every 100 ms as batch updates
        if (!key_move_timer_is_active && point) {
            send_key_move_timer = setInterval(function() {
                if (key_move_delta) {
                    var itemNames = new Array();
                    for (x in paper.project.selectedItems) {
                        var item = paper.project.selectedItems[x];
                        itemNames.push(item._name);
                    }
                    socket.emit('item:move:progress', room, uid, itemNames, key_move_delta);
                    key_move_delta = null;
                }
            }, 100);
        }
        key_move_timer_is_active = true;
    }
}

function onKeyUp(event) {
    if (event.key === "delete") {
        deleteSelectedItem();
    }

    if (activeTool == "select") {
        // End arrow key movement timer
        clearInterval(send_key_move_timer);
        if (key_move_delta) {
            // Send any remaining movement info
            var itemNames = new Array();
            for (x in paper.project.selectedItems) {
                var item = paper.project.selectedItems[x];
                itemNames.push(item._name);
            }
            socket.emit('item:move:end', room, uid, itemNames, key_move_delta, currentPageNumber);
        } else {
            // delta is null, so send 0 change
            socket.emit('item:move:end', room, uid, itemNames, new Point(0, 0), currentPageNumber);
        }
        key_move_delta = null;
        key_move_timer_is_active = false;
    }
}

function moveItemsBy1Pixel(point) {
    if (!point) {
        return;
    }

    if (paper.project.selectedItems.length < 1) {
        return;
    }

    // Move locally
    var itemNames = new Array();
    for (x in paper.project.selectedItems) {
        var item = paper.project.selectedItems[x];
        item.position += point;
        itemNames.push(item._name);
    }

    // Redraw screen for item position update
    view.draw();
}

// Drop image onto canvas to upload it
$('#myCanvas').bind('dragover dragenter', function(e) {
    e.preventDefault();
});

$('#myCanvas').bind('drop', function(e) {
    e = e || window.event; // get window.event if e argument missing (in IE)
    if (e.preventDefault) { // stops the browser from redirecting off to the image.
        e.preventDefault();
    }
    e = e.originalEvent;
    var dt = e.dataTransfer;
    var files = dt.files;
    for (var i = 0; i < files.length; i++) {
        var file = files[i];
        uploadImage(file);
    }
});

// CONTROLS EVENTS
var $color = $('.colorSwatch:not(#pickerSwatch)');
$color.on('click', function() {
    $color.removeClass('active');
    $(this).addClass('active');
    $('#activeColorSwatch').css('background-color', $(this).css('background-color'));
    update_active_color();
});

/*Load New Page*/
$('#load-new-pg').click(function() {
    updateStylingFromTools();
    redoStack.length = 0;

    pageCount++;
    clearCanvas();
    socket.emit('load:newPage', room, pageCount, pageCount);
    currentPageNumber = pageCount;
});

/*Load Next Page*/
$('.load-next-pg').click(function() {
    updateStylingFromTools();
    if (currentPageNumber <= pageCount) {
        redoStack.length = 0;
        socket.emit('load:previousPage', room, currentPageNumber + 1);
    } else
        alert("You have reached the last page");
});

/*Loading Previous Page*/
$('.load-previous-pg').click(function() {
    updateStylingFromTools();
    if (currentPageNumber > 1) {
        redoStack.length = 0;
        socket.emit('load:previousPage', room, currentPageNumber - 1);
    } else
        alert("You have reached the first page");
});

$('#exportAsImage').on('click', function() {
    if (redoStack.length > 0) { // clearing redo stack after user restarts drawing
        $('.buttonicon-redo').addClass('disabled'); // notifying user that he can't redo now
        redoStack.length = 0;
    }
});

$('#exportSVG').on('click', function() {
    var svg = paper.project.exportSVG();
    encodeAsImgAndLink(svg);
});

$('#exportPNG').on('click', function() {
    var rect = new Path.Rectangle({ //Set background first
        point: [0, 0],
        size: [view.size.width , view.size.height],
        strokeColor: 'white',
        selected: true
    });
    rect.sendToBack();
    rect.fillColor = '#ffffff';
    view.draw();

    this.href = document.getElementById('myCanvas').toDataURL("image/png");
    $('#importexport').modal('hide');
});

$('#text-box-save').on('click', function() {
    var text = $('#text-box').val().trim();
    var fontColor = $('select[name="font-colors"]').val();
    var fontSize = $('select[name="font-sizes"]').val();
    if (text.length > 0) {
        var text = new PointText({
            point: textBoxCoordinate,
            content: text,
            fillColor: fontColor,
            fontSize: fontSize
        });
        text.name = uid + ":" + (++paper_object_count);
        socket.emit('add:textbox', room, uid, text, fontColor, fontSize, textBoxCoordinate, text.name, currentPageNumber);
        textBoxCoordinate = null;
        $('#text-box').val(''); // empty the text box
        $('#whiteboard-text-box-container').hide();
    }
    paper.project.activeLayer.selected = false;
});

$('#text-box-cancel').on('click', function() {
    textBoxCoordinate = null;
    $('#text-box').val(''); // empty the text box
    $('#whiteboard-text-box-container').hide();
});

$('#clearTool').on('click', function() {
    activeTool = "pencil";
    $('#myCanvas').css('cursor', 'pointer');
    clearCanvas();
    socket.emit('clear', room, uid, currentPageNumber);
});

$('#deleteTool').on('click', function() {
    deleteSelectedItem();
    $('#deleteTool').hide();
});

$('#undoTool').on('click', function() {
    if (paper.project.activeLayer.hasChildren()) {
        $('.buttonicon-redo').removeClass('disabled');
        activeTool = "undo";
        redoStack.push(paper.project.activeLayer.lastChild);
        socket.emit('undo', room, uid, currentPageNumber);
        paper.project.activeLayer.lastChild.remove();
        if (!paper.project.activeLayer.hasChildren())
            $('.buttonicon-undo').addClass('disabled');
        view.draw();
    }
});

$('#redoTool').on('click', function() {
    if (redoStack.length > 0) {
        $('.buttonicon-undo').removeClass('disabled');
        activeTool = "redo";
        socket.emit('redo', room, uid, currentPageNumber);
        paper.project.activeLayer.addChild(redoStack.pop());
        if (redoStack.length == 0)
            $('.buttonicon-redo').addClass('disabled');
        view.draw();
    }
});

$('#canvasClear').on('click', function() {
    clearCanvas();
});

function editTool(parent, type, trigger, cursor, callback) {
    $(trigger).on('click', function() {
        if (type === 'pencil' || type === 'eraser') {
            $('#tipInput').show();
        } else {
            $('#tipInput').hide();
        }
        updateStylingFromTools('button[title="' + parent + '"]', type);
        $('#myCanvas').css('cursor', cursor);
        activeTool = type;
        if (callback)
            callback();
        paper.project.activeLayer.selected = false;
    });
}

function strokeSetup() {
    document.getElementById('tip').value = 5;
}

function fitSetup() {
    scalePercent = 1;
    paper.view.zoom = 1;
    paper.view.center = defaultCenter;
    document.getElementById('scale-percent').textContent = "100%";
}

editTool(null, 'select', '#selectTool', 'default')
editTool('Basic Tools', 'draw', '#drawTool', 'pointer');
editTool('Basic Tools', 'pencil', '#pencilTool', 'pointer', strokeSetup);
editTool('Basic Tools', 'eraser', '#eraserTool', 'url(/wb_assets/static/img/cursor_eraser.png),pointer', strokeSetup);
editTool('Basic Tools', 'point', '#pointTool', 'pointer');
editTool('Basic Tools', 'textBox', '#textBoxTool', 'text');

editTool('Shapes', 'line', '#lineTool', 'pointer');
editTool('Shapes', 'rectangle', '#rectangleTool', 'pointer');
editTool('Shapes', 'triangle', '#triangleTool', 'pointer');
editTool('Shapes', 'circle', '#circleTool', 'pointer');

editTool('Zoom', 'zoom-in', '#zoomInTool', 'zoom-in');
editTool('Zoom', 'zoom-out', '#zoomOutTool', 'zoom-out');
editTool('Zoom', 'zoom-fit', '#zoomFitTool', 'default', fitSetup);

function clearCanvas() {
    // Remove all but the active layer
    if (project.layers.length > 1) {
        var activeLayerID = project.activeLayer._id;
        for (var i = 0; i < project.layers.length; i++) {
            if (project.layers[i]._id != activeLayerID) {
                project.layers[i].remove();
                i--;
            }
        }
    }

    // Remove all of the children from the active layer
    if (paper.project.activeLayer && paper.project.activeLayer.hasChildren()) {
        paper.project.activeLayer.removeChildren();
    }
    $('.buttonicon-undo').addClass('disabled');
    $('.buttonicon-redo').addClass('disabled');
    view.draw();
}

// Encodes svg as a base64 text and opens a new browser window
// to the svg image that can be saved as a .svg on the users
// local filesystem. This skips making a round trip to the server
// for a POST.
function encodeAsImgAndLink(svg) {
    if ($.browser.msie) {
        // Add some critical information
        svg.setAttribute('version', '1.1');
        var dummy = document.createElement('div');
        dummy.appendChild(svg);
        window.winsvg = window.open('/static/html/export.html');
        window.winsvg.document.write(dummy.innerHTML);
        window.winsvg.document.body.style.margin = 0;
    } else {
        // Add some critical information
        svg.setAttribute('version', '1.1');
        svg.setAttribute('xmlns', 'http://www.w3.org/2000/svg');
        svg.setAttribute('xmlns:xlink', 'http://www.w3.org/1999/xlink');

        var dummy = document.createElement('div');
        dummy.appendChild(svg);

        var b64 = Base64.encode(dummy.innerHTML);

        //window.winsvg = window.open("data:image/svg+xml;base64,\n"+b64);
        var html = "<img style='height:100%;width:100%;' src='data:image/svg+xml;base64," + b64 + "' />"
        window.winsvg = window.open();
        window.winsvg.document.write(html);
        window.winsvg.document.body.style.margin = 0;
    }
}

var rasterNo = 0;
var previousRasterWidth = 0,
    previousRasterHeight = 0;

function configRaster(raster) {
    if (raster.width == 0 || raster.height == 0) {
        raster.position = view.center
    } else {
        raster.position = [raster.width + previousRasterWidth, raster.height / 2 + previousRasterHeight];
    }
    previousRasterWidth += raster.width;
    previousRasterHeight += raster.height;
    raster.name = uid + ":" + (++paper_object_count);
}

function uploadImage(file) {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    $(reader).bind('loadend', function(e) {
        var bin = this.result;
        var raster = new Raster(bin);
        configRaster(raster)
        $('.buttonicon-undo').removeClass('disabled');
        socket.emit('image:add', room, uid, JSON.stringify(bin), raster.position, raster.name, currentPageNumber);
    });
}

$('#imgCropped').on('click', function() {
    getCroppedImage();
    if (croppedImg != null) {
        var raster = new Raster(croppedImg);
        configRaster(raster)
        socket.emit('image:add', room, uid, JSON.stringify(croppedImg), raster.position, raster.name, currentPageNumber);
    }
    resetCropper();
});

$('#openImg').on('click', function() {
    var files = document.getElementById('croppingImgUpload').files;
    for (var i = 0; i < files.length; i++) {
        var file = files[i];
        uploadImage(file);
    }
    if (redoStack.length > 0) { // clearing redo stack after user restarts drawing
        $('.buttonicon-redo').addClass('disabled'); // notifying user that he can't redo now
        redoStack.length = 0;
    }
    resetCropper();
});

$('#croppingImgUpload').on('change', function() {
    updateCropperCanvas(document.getElementById("croppingImgUpload"));
});

function setPageToolsCSS(currentPageNumber) {
    if (currentPageNumber != 1) {
        $('.load-previous-pg').removeClass('disabled');
    } else
        $('.load-previous-pg').addClass('disabled');
    if (currentPageNumber <= pageCount) {
        $('.load-next-pg').removeClass('disabled');
    } else
        $('.load-next-pg').addClass('disabled');
}

function updateWhiteboardPageNumber() {
    if (pageCount > 1) {
        $('.p_nbr').remove();
        $('.whiteboard-page-number-container').show();
        for (var _p = 1; _p <= pageCount; _p++) {
            $('#pageSelector').append('<option id="whiteboard-page-num-' + _p + '" class="p_nbr" value="' + _p + '">' + _p + '</option>');
        }
        document.getElementById('pageSelector').value = currentPageNumber
        document.getElementById('whiteboard-page-count').textContent = pageCount;
    }
}

$('#pageSelector').on('input', function() {
    currentPageNumber = (this).value;
    socket.emit('load:previousPage', room, currentPageNumber);
});
// ---------------------------------
// SOCKET.IO EVENTS
socket.on('settings', function(settings) {
    processSettings(settings);
});

socket.on('draw:progress', function(artist, data, stroke_width) {
    // It wasn't this user who created the event
    if (artist !== uid && data) {
        progress_external_path(JSON.parse(data), artist, stroke_width);
    }

});

socket.on('draw:end', function(artist, data) {
    // It wasn't this user who created the event
    if (artist !== uid && data) {
        end_external_path(JSON.parse(data), artist);
    }
});

socket.on('user:connect', function(users) {
    update_user_count(users);
});

socket.on('user:disconnect', function(users) {
    update_user_count(users);
});

// loading a whiteboard page from db
socket.on('project:load', function(json, pgCount, currentPgNum) {
    paper.project.activeLayer.remove();
    paper.project.importJSON(json.project);
    pageCount = pgCount;
    currentPageNumber = currentPgNum;
    updateWhiteboardPageNumber();
    setPageToolsCSS(currentPageNumber);
    $('#opacityRangeVal').on('touchstart MSPointerDown mousedown', function(ev) {
        ev.stopPropagation();
    }).on('change', function(ev) {
        update_active_color();
    });

    view.draw();
});

socket.on('project:load:error', function() {
    $('#lostConnection').show();
});

socket.on('load:newPage', function(pageNum, clearedCount) {
    currentPageNumber = clearedCount;
    pageCount = pageNum;
    redoStack.length = 0;
    updateWhiteboardPageNumber();
    setPageToolsCSS(currentPageNumber);
    clearCanvas();
});

socket.on('clear', function(artist) {
    if (artist != uid) {
        clearCanvas();
    }
});

socket.on('loading:start', function() {
    $('#loading').show();
});

socket.on('loading:end', function() {
    $('#loading').hide();
    $('#colorpicker').farbtastic(pickColor); // make a color picker
    // cake
    $('#canvasContainer').css("background-image", 'none');

});

socket.on('load:previousPage', function(json, previousPageNumber) {
    currentPageNumber = previousPageNumber;
    redoStack.length = 0;
    updateWhiteboardPageNumber();
    setPageToolsCSS(previousPageNumber);
    paper.project.activeLayer.remove();
    paper.project.importJSON(json.project);
    $('#opacityRangeVal').on('touchstart MSPointerDown mousedown', function(ev) {
        ev.stopPropagation();
    }).on('change', function(ev) {
        update_active_color();
    });

    view.draw();
});

socket.on('item:remove', function(artist, name) {
    if (artist != uid && paper.project.activeLayer._namedChildren[name][0]) {
        paper.project.activeLayer._namedChildren[name][0].remove();
        view.draw();
    }
});

socket.on('item:move', function(artist, itemNames, delta) {
    if (artist != uid) {
        for (x in itemNames) {
            var itemName = itemNames[x];
            if (paper.project.activeLayer._namedChildren[itemName] && paper.project.activeLayer._namedChildren[itemName][0]) {
                paper.project.activeLayer._namedChildren[itemName][0].position += new Point(delta[1], delta[2]);
            }
        }
        view.draw();
    }
});

socket.on('image:add', function(artist, data, position, name) {
    if (artist != uid) {
        var image = JSON.parse(data);
        var raster = new Raster(image);
        raster.position = new Point(position[1], position[2]);
        raster.name = name;
        view.draw();
    }
});

socket.on('undo', function(artist) {
    if (artist != uid) {
        redoStack.push(paper.project.activeLayer.lastChild);
        paper.project.activeLayer.lastChild.remove();
        view.draw();
    }
});

socket.on('redo', function(artist) {
    if (artist != uid) {
        paper.project.activeLayer.addChild(redoStack.pop());
        view.draw();
    }
});

socket.on('image:resize', function(artist, imageName, scalingFactor) {
    if (paper.project.activeLayer._namedChildren[imageName][0]) {
        paper.project.activeLayer._namedChildren[imageName][0].scale(scalingFactor);
        view.draw();
    }
});

socket.on('pointing:start', function(artist, position) {
    if (artist != uid) {
        // reduced few pixels to adjust the cursor position perfectly
        $('#dummy-cursor').css({
            "top": (position[2] - 2) + 'px',
            "left": (position[1] - 10) + 'px',
            "display": "block"
        });
        view.draw();
    }
});

socket.on('pointing:end', function(artist, position) {
    if (artist != uid) {
        $('#dummy-cursor').css({
            "display": "none"
        });
        view.draw();
    }
});

socket.on('add:textbox', function(artist, text, fontColor, fontSize, position, name) {
    if (artist != uid) {
        var text = new PointText({
            point: new Point(position[1], position[2]),
            content: text[1].content,
            fillColor: fontColor,
            fontSize: fontSize,
        });
        text.name = name
        view.draw();
    }
});

socket.on('enable:toolbox', function(artist) {
    if (artist != uid) {
        $('.tool-box').css({
            "display": "block"
        });
        $('.tool-box').removeClass('animated bounceOut');
        $('.tool-box').addClass('animated zoomIn');
    }
});

socket.on('disable:toolbox', function(artist) {
    if (artist != uid) {
        $('.tool-box').removeClass('animated zoomIn');
        $('.tool-box').addClass('animated bounceOut');
    }
});

//scroll event
socket.on('scroll', function(artist, yPosition) {
    if (artist != uid) {
        previousYPosition = yPosition;
        $('#subContainer').scrollTop(yPosition);
    }
});

function requestFullScreen(element) {
    // Supports most browsers and their versions.
    var requestMethod = element.requestFullScreen || element.webkitRequestFullScreen || element.mozRequestFullScreen || element.msRequestFullscreen;

    if (requestMethod) { // Native full screen.
        requestMethod.call(element);
    } else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
        var wscript = new ActiveXObject("WScript.Shell");
        if (wscript !== null) {
            wscript.SendKeys("{F11}");
        }
    }
}

// --------------------------------- 
// SOCKET.IO EVENT FUNCTIONS

// Updates the active connections
function update_user_count(users) {
    $('li').remove('.nav-item');
    var obj = {};

    for (var i = 0, len = users.length; i < len; i++)
        obj[users[i]['name']] = users[i];

    users = new Array();
    for (var key in obj)
        users.push(obj[key]);

    for (element in users) {
        // $('#user-list').append('<li class="nav-item"><a class="nav-link" href="#"><span data-feather="file-text"></span>' + users[element].name + '</a></li>');
    }
}

var external_paths = {};

// Ends a path
var end_external_path = function(points, artist) {
    prevpath = null;
    var path = external_paths[artist];
    if (path) {
        path.closed = true;
        external_paths[artist] = false;
    }
};

// Continues to draw a path in real time
var prevpath = null;
var progress_external_path = function(points, artist, stroke_width) {
    var color = buildRGBAColor(points.rgba.red, points.rgba.green, points.rgba.blue, points.rgba.opacity);

    if (points.tool == "line") {
        if (prevpath) {
            prevpath.remove();
        }
        path = external_paths[artist];
        if (!path) {
            //  // Creates the path in an easy to access way
            external_paths[artist] = new Path();
            path = external_paths[artist];
        }
        var line = new Path.Line(new Point(points.path.start[1], points.path.start[2]), new Point(points.path.end[1], points.path.end[2]));
        line.strokeColor = color;
        line.strokeWidth = 2;
        line.name = points.name;
        path = line;
        prevpath = path;


    } else if (points.tool == "rectangle") {
        if (prevpath) {
            prevpath.remove();
        }
        path = external_paths[artist];

        if (!path) {
            // Creates the path in an easy to access way
            external_paths[artist] = new Path();
            path = external_paths[artist];
        }
        var rectangle = new Path.Rectangle(new Point(points.path.start[1], points.path.start[2]), new Point(points.path.end[1], points.path.end[2]));
        rectangle.strokeColor = color;
        rectangle.strokeWidth = 2;
        rectangle.name = points.name;
        path = rectangle;
        prevpath = path;
    } else if (points.tool == "triangle") {
        if (prevpath) {
            prevpath.remove();
        }
        path = external_paths[artist];

        if (!path) {
            // Creates the path in an easy to access way
            external_paths[artist] = new Path();
            path = external_paths[artist];
        }
        var triangle = new Path();
        triangle.add(new Point(points.path.start[1], points.path.start[2]));
        triangle.add(new Point(points.path.end[1], points.path.end[2]));
        triangle.add(new Point(2 * points.path.start[1] - points.path.end[1], points.path.end[2]));
        triangle.closed = true;
        triangle.strokeColor = color;
        triangle.strokeWidth = 2;
        triangle.name = points.name;
        path = triangle;
        prevpath = path;
    } else if (points.tool == "circle") {
        if (prevpath) {
            prevpath.remove();
        }
        path = external_paths[artist];

        if (!path) {
            // Creates the path in an easy to access way
            external_paths[artist] = new Path();
            path = external_paths[artist];
        }
        var circle = new Path.Circle(new Point((points.path.start[1] + points.path.end[1]) / 2, (points.path.start[2] + points.path.end[2]) / 2), (new Point(points.path.start[1], points.path.start[2]) - new Point(points.path.end[1], points.path.end[2])).length / 2);
        circle.strokeColor = color;
        circle.strokeWidth = 2;
        circle.name = points.name;
        path = circle;
        prevpath = path;
    } else {
        var path = external_paths[artist];

        // The path hasn't already been started
        // So start it
        if (!path) {

            // Creates the path in an easy to access way
            external_paths[artist] = new Path();
            path = external_paths[artist];

            // Starts the path
            var start_point = new Point(points.start[1], points.start[2]);

            if (points.tool == "draw") {
                path.fillColor = color;
            } else if (points.tool == "pencil") {
                path.strokeColor = color;
                path.strokeWidth = stroke_width;
            } else if (points.tool == "eraser") {
                path.strokeColor = color;
                path.strokeWidth = stroke_width;
            }
            if (points.tool == "line") {
                //start_point = new Line();
                //path.add(points.path);
            } else {
                path.name = points.name;
                path.add(start_point);
            }

        }

        // Draw all the points along the length of the path
        var paths = points.path;
        var length = paths.length;
        for (var i = 0; i < length; i++) {

            path.add(new Point(paths[i].top[1], paths[i].top[2]));
            path.insert(0, new Point(paths[i].bottom[1], paths[i].bottom[2]));

        }
    }

    view.draw();

};

// Continues to draw a path in real time
function processSettings(settings) {

    $.each(settings, function(k, v) {

        // Handle tool changes
        if (k === "tool") {
            $('.buttonicon-' + v).click();
        }

    })

}