/**
 * Created by thilina on 8/17/16.
 */
var croppedImg = null;

$('.img-container > img').cropper({
    viewMode: 2,
    dragMode: 'move',
});

getCroppedImage = () => {
    var cropBox = $('#croppingImg').cropper('getCropBoxData');
    var canvas; 
    if (cropBox.width != null){
        canvas = $('#croppingImg').cropper('getCroppedCanvas',{
            width: cropBox.width,
            height: cropBox.height
        });
        croppedImg = canvas.toDataURL();
        $('#imgCroppingModal').modal('hide');
    }
    else{
        croppedImg = null;
        alert("You can't crop an empty image! Please upload one!")
    }
}

updateCropperCanvas = (input) => {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
    
        reader.onload = function (e) {
            // $('#croppingImg').attr('src', e.target.result);
            $('#croppingImg').cropper('replace', e.target.result);
            $('#croppingImg').cropper('getCroppedCanvas');
        };
        reader.readAsDataURL(input.files[0]);
    }
}

resetCropper = () => {
    var currentCropper = $('#croppingImg').data('cropper');
    $('#croppingImg').removeAttr('src');
    currentCropper.destroy();
    $('#croppingImgUpload').val('');
    $('#croppingImg').data('cropper', null);
}