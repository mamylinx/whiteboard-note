This project was originally forked from [Ether Draw](https://github.com/JohnMcLear/draw) and developed by Rakotomalala Mamy Tiana.

WHITEBOARD
================
This is the collaborative drawing tool.This project was initiated using [etherdraw] which is released under Apache License

Make sure you have Node 10 or above.

Installation
------------
  1. Install Requirements. ``sudo apt-get update && sudo apt-get install git libcairo2-dev libjpeg62-turbo-dev libpango1.0-dev libgif-dev build-essential g++``
  2. Enter the whiteboard folder `` cd whiteboard ``
  3. For the first time use, install node dependencies and run whiteboard `` bin/run.sh `` 
  Once dependencies installed, you can use `` node whiteboard.js`` to run whiteboard. 
  4. Make a drawing!  Open your browser and visit `` http://127.0.0.1:9002?username=<username_here> ``
  ``username`` is mandatory. If not, whiteboard will throw 404 error.